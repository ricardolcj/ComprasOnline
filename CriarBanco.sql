create database trabalho;

use trabalho;
create table cliente(
/*CPF e telefone não cabem em um int, então usei char. Email é usado como login para os
clientes WEB, então usei UNIQUE para garantir que é uma chave candidata*/
	cpf char(12) not null,
	nome varchar(40) not null,
	bairro varchar(20) not null,
	cidade varchar(20) not null,
	estado varchar(20) not null,
	email varchar(30) null UNIQUE,
	status varchar(20) not null,
	telefone varchar(12) not null,
	primary key (cpf)
);

create table produto(
	id integer not null,
	nome varchar(20) not null,
	preco real not null,
	primary key (id)
);

create table conta(
	/*Como o relacionamento entre conta e cliente é de "um para um", a chave estrangeira
	poderia estar em qualque uma das duas tabelas. A cláusula on delete cascade serve para,
	em caso de deleção da chave na tabela cliente, excluir também na conta.*/
	id char(12) not null,
	id_cliente char(12) not null,
	foreign key (id_cliente) references cliente (cpf) on delete cascade,
	primary key (id)
);

create table carrinho(
	/*Assim como na tabela anterior, no caso de exclusão de um cliente, on delete cascade
	exclui também o carrinho associado.
	Como uma conta PODE ter um carrinho (se for cliente WEB), o relacionamento fica
	representado com a chave estrangeira em carrinho.*/
	id char(12) not null,
	id_conta char(12) not null,
	foreign key (id_conta) references conta (id) on delete cascade,
	primary key (id)
);

create table pedido(
	/*Como uma conta pode ter nenhum ou vários pedidos, a chave estrangeira fica aqui.
	On delete cascade pela mesma razão explicada anteriormente.*/
	id integer not null,
	id_conta char(12) not null,
	status varchar(20) null,
	foreign key (id_conta) references conta (id) on delete cascade,
	primary key (id)
);

create table pagamento(
	/*Como um pedido pode ter nenhum a vários pagamentos, o relacionamento fica
	representado aqui.*/
	id integer not null,
	metodo varchar(20) not null,
	valor real not null,
	id_pedido integer not null,
	foreign key (id_pedido) references pedido (id) on delete cascade,
	primary key (id)
);

create table item(
	/*Como im item necessariamaente esta em um pedido OU em um carrinho, 
	id_pedido e id_carrinho podem ser NULL. Quando um produto for excluído
	do sistema, id_produto assume NULL, pois não podemos, por exemplo, excluir
	um item de um pedido já realizado. Como temos preco_unit, não perdemos
	o preço antigo do produto em pedidos.*/
	id integer not null,
	quantidade integer not null,
	id_produto integer null,
	preco_unit real NULL,
	foreign key (id_produto) references produto (id) on delete set null,
	id_pedido integer null,
	foreign key (id_pedido) references pedido (id) on delete cascade,
	id_carrinho char(12) null,
	foreign key (id_carrinho) references carrinho (id) on delete cascade,
	primary key (id)
);