#include <stdio.h>
#include <mysql/mysql.h>
#include <string.h>
/* Variáveis de Inserção/Consulta/Atualização/Remoção para Cliente*/
long int id_cliente, telefone;
char nome_cliente[40], bairro[20], cidade[20], estado[20];
char email[30], status[20];

/* Variáveis de Inserção/Consulta/Atualização/Remoção para Conta*/
long int id_conta;

/* Variáveis de Inserção/Consulta/Atualização/Remoção para Produto*/
int id_produto;
char nome_produto[20];
float preco;

/* Variáveis de Inserção/Consulta/Atualização/Remoção para Carrinho*/
long int id_carrinho;

/* Variáveis de Inserção/Consulta/Atualização/Remoção para Pagamento*/
int id_pagamento;
float valor_pago;
char metodo[20];

/* Variáveis de Inserção/Consulta/Atualização/Remoção para Pedido*/
int id_pedido;
char status_pedido[20];

/* Variáveis de Inserção/Consulta/Atualização/Remoção para Item*/
int id_item, quantidade;

/*Definindo ponteiros utilizados na conexão com o BD*/
MYSQL *conn;
MYSQL_RES *res;
MYSQL_ROW row;

char *server = "localhost";
char *user = "root";
char *password = "";
char *database = "trabalho";

void consulta(char* string, int print){
	/*Consulta ao BD*/
	if (mysql_query(conn, string)) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		exit(1);
	}

	res = mysql_use_result(conn);

	/* Loop para printar o Resultado da consulta na tela*/
	if(print){
		while ((row = mysql_fetch_row(res)) != NULL){
			printf("%s", row[0]);
			for(int i = 1; i < mysql_num_fields(res); i++){
				printf(" %s", row[i]);
			}
			printf("\n");
		}
	}
}

int MenuPrincipal(){
	int n;
	
	printf("SISTEMA PARA COMPRAS ONLINE\n");
	printf("O que deseja fazer?\n\n");
	printf("1. Inserir\n");
	printf("2. Consultar\n");
	printf("3. Atualizar\n");
	printf("4. Remover\n");
	printf("5. Sair\n");

	scanf("%d", &n);
	system("clear");

	return n;
}

int MenuInserir(){
	system("clear");
	int n;

	printf("SISTEMA PARA COMPRAS ONLINE\n");
	printf("O que deseja inserir?\n\n");
	printf("1. Novo Cliente\n");
	printf("2. Novo Produto\n");
	printf("3. Novo Pagamento\n");
	printf("4. Novo Pedido\n");
	printf("5. Novo Item\n");

	scanf("%d", &n);
	system("clear");

	return n;
}

int MenuConsultar(){
	system("clear");
	int n;
	
	printf("SISTEMA PARA COMPRAS ONLINE\n");
	printf("O que deseja Consultar?\n\n");
	printf("1. Pedidos associados a uma conta\n");
	printf("2. Produtos em um Carrinho de compras\n");
	printf("3. Quantidade e nome dos usuários cadastrados no sistema\n");
	printf("4. Forma de pagamento mais utilizada\n");
	printf("5. Filtrar usuários por bairro cidade e estado\n");
	printf("6. Valor total de um carrinho\n");
	printf("7. Valor pago por um pedido\n");
	printf("8. Quantidade de pagamentos feitos a um pedido\n");
	printf("9. Quantidade de itens de um pedido\n");
	printf("10. Nome dos clientes que compram online\n");
	printf("11. Total de clientes ativos\n");
	printf("12. Estado com mais clientes\n");
	printf("13. Cliente com mais pedidos\n");
	printf("14. Produto mais comprado\n");
	printf("15. Tíquete Médio (valor médio dos pedidos)\n");
	
	scanf("%d", &n);
	system("clear");
	
	return n;
}

int MenuAtualizar(){
	system("clear");
	int n;

	printf("SISTEMA PARA COMPRAS ONLINE\n");
	printf("O que deseja alterar?\n\n");
	printf("1. Preço de um produto\n");
	printf("2. Quantidade de um item no carrinho\n");
	printf("3. Status de um cliente\n");
	printf("4. Status de um pedido\n");
	printf("5. Endereço de um cliente\n");

	scanf("%d", &n);
	system("clear");

	return n;

}

int MenuRemover(){
	system("clear");
	int n;

	printf("SISTEMA PARA COMPRAS ONLINE\n");
	printf("O que deseja remover?\n\n");
	printf("1. Clientes que nunca fizeram pedido e tem carrinho vazio\n");
	printf("2. Todos os itens de um carrinho\n");
	printf("3. Um produto\n");
	printf("4. Um cliente\n");

	scanf("%d", &n);
	system("clear");

	return n;
}

int main(int argc, char const *argv[])
{
	conn = mysql_init(NULL);
	int n = -1;
	char insert [2000], aux, aux2[15];

	/*Faz a conexão com o Banco de Dados*/
	if (!mysql_real_connect(conn, server, user, password, database, 0, NULL, 0)) {
		fprintf(stderr, "%s\n", mysql_error(conn));
		exit(1);
	}

	while(n!=0){

		system("clear");
		
		n = MenuPrincipal();

		if (n == 5) break;

		else if (n == 1){ /*INSERÇÃO*/
		
			n = MenuInserir();
		
			switch (n){

				case 1:/*Inserir Cliente*/
					printf("CPF do cliente: ");
					scanf("%ld", &id_cliente);
					printf("nome do cliente: ");
					scanf("%*c%[^\n]", nome_cliente);
					printf("Bairro: ");
					scanf("%*c%[^\n]", bairro);
					printf("Cidade: ");
					scanf("%*c%[^\n]", cidade);
					printf("Estado: ");
					scanf("%*c%[^\n]", estado);
					printf("telefone (somente números): ");
					scanf("%ld", &telefone);
					printf("status do cliente: ");
					scanf("%*c%[^\n]", status);

					k:
					printf("O cliente é usuário WEB? [s/n]: ");
					scanf("%*c%c", &aux);
					if(aux=='s'){/*Se for cliente WEB, tem que ter carrinho e email*/
						printf("email do cliente: ");
						scanf("%s", email);
						sprintf(insert,"insert into cliente values ('%ld', '%s', '%s', '%s', '%s', '%s', '%s', %ld);", id_cliente, nome_cliente, bairro, cidade, estado, email, status, telefone);
						consulta(insert, 0);
						sprintf(insert, "insert into conta values ('%ld', '%ld');", id_cliente, id_cliente);
						consulta(insert, 0);
						sprintf(insert, "insert into carrinho values ('%ld', '%ld');", id_cliente, id_cliente);
						consulta(insert, 0);

					}
					else if(aux=='n'){
						sprintf(insert,"insert into cliente (cpf, nome, bairro, cidade, estado, status, telefone) values ('%ld', '%s', '%s', '%s', '%s', '%s', %ld);", id_cliente, nome_cliente, bairro, cidade, estado, status, telefone);
						consulta(insert, 0);
						sprintf(insert, "insert into conta values ('%ld', '%ld');", id_cliente, id_cliente);
						consulta(insert, 0);
					}
					else goto k;
					printf("\nCliente cadastrado com sucesso\n");

				break;

				case 2:/*Inserir Produto*/
					printf("id do produto: ");
					scanf("%d", &id_produto);
					printf("preco do produto: ");
					scanf("%f", &preco);
					printf("nome do produto: ");
					scanf("%*c%[^\n]", nome_produto);
					/*Cadastra um produto*/
					sprintf(insert, "insert into produto values ('%d', '%s', '%f');", id_produto, nome_produto, preco);
					consulta(insert, 0);
					printf("\nProduto cadastrado com sucesso\n");

				break;

				case 3:/*Inserir Pagamento*/
					printf("id do pagamento: ");
					scanf("%d", &id_pagamento);
					printf("método de pagamento: ");
					scanf("%*c%[^\n]", metodo);
					printf("valor pago: ");
					scanf("%f", &valor_pago);
					printf("id do pedido: ");
					scanf("%d", &id_pedido);

					sprintf(insert, "insert into pagamento values ('%d', '%s', '%f', '%d');", id_pagamento, metodo, valor_pago, id_pedido);
					consulta(insert, 0);
					printf("\nPagamento cadastrado com sucesso\n");
				break;

				case 4:/*Inserir Pedido*/
					printf("id do pedido: ");
					scanf("%d", &id_pedido);
					printf("id da conta: ");
					scanf("%ld", &id_conta);
					sprintf(insert, "insert into pedido (id, id_conta) values ('%d', '%ld');", id_pedido, id_conta);
					consulta(insert, 0);
					printf("\nPedido adicionado com sucesso");
				break;

				case 5:/*Adicionar um Item a um pedido ou a um carrinho*/
					printf("id do item: ");
					scanf("%d", &id_item);
					printf("id do produto: ");
					scanf("%d", &id_produto);
					printf("quantidade: ");
					scanf("%d", &quantidade);

					l:

					printf("Adicionar item à um carrinho ou à um pedido? [c/p]: ");
					scanf("%*c%c", &aux);
					if(aux=='c'){
						printf("id do carrinho: ");
						scanf("%ld", &id_carrinho);
						sprintf(insert,"insert into item (id, quantidade, id_produto, id_carrinho) values ('%d', '%d', '%d', '%ld');", id_item, quantidade, id_produto, id_carrinho);
						consulta(insert, 0);
					}
					else if(aux=='p'){/*Item em pedidos não sofrem variação de preço, por isso depois de adicionar o item, altero o campo
										preço_unit para armazenar o preço do produto no momento da compra*/
						printf("id do pedido: ");
						scanf("%d", &id_pedido);
						sprintf(insert,"insert into item (id, quantidade, id_produto, id_pedido) values ('%d', '%d', '%d', '%d');", id_item, quantidade, id_produto, id_pedido);        
						consulta(insert, 0);
						sprintf(insert, "update item, produto set item.preco_unit=produto.preco where item.id_produto=produto.id and item.id='%d';", id_item);
						consulta(insert,0);
					
					}
					else goto l;
					printf("\nItem adicionado com sucesso\n");
				break;

			}
		}
		else if(n == 2){
	
			n = MenuConsultar();

			switch (n){
				case 1:/*Pedidos associados a determinada conta*/
					printf("id da conta: ");
					scanf("%ld", &id_conta);
					sprintf(insert, "select id from pedido where id_conta like'%ld';", id_conta);
					consulta(insert, 1);
				break;

				case 2:/*Produtos em determinado carrinho de compras: mostra os nomes dos itens que tem id_carrinho*/
					printf("id do carrinho: ");
					scanf("%ld", &id_carrinho);
					sprintf(insert, "select nome from produto where id in (select id_produto from item where id_carrinho like'%ld');", id_carrinho);
					consulta(insert, 1);
				break;
			
				case 3:/*Quantidade e nome dos usuários no sistema*/
					printf("Quantidade de usuários: ");
					consulta("select count(cpf) from cliente;", 1);
					printf("\nUsuários:\n");
					consulta("select distinct cpf, nome from cliente;", 1);
				break;

				case 4:/*Forma de pagamento mais utilizada: agrupa as formas de pagamento, ordena de dorma decrescente e pega a primeira*/
					consulta("select metodo from pedido left outer join pagamento on pedido.id=pagamento.id_pedido group by metodo order by count(*) desc limit 1;", 1);
				break;

				case 5:/*Filtrar usuários por bairro, cidade e estado*/
					printf("Bairro: ");
					scanf("%*c%[^,\n]", bairro);
					printf("Cidade: ");
					scanf("%*c%[^,\n]", cidade);
					printf("Estado: ");
					scanf("%*c%[^,\n]", estado);
					sprintf(insert, "select cpf, nome from cliente where bairro like'%s' and cidade like'%s' and estado like'%s';", bairro, cidade, estado);
					consulta(insert, 1);
				break;

				case 6:/*Valor total de um carrinho: join de item e produto. pega todos os itens do carrinho e multiplica as quantidade pelos precos atualizado*/
					printf("id do carrinho: ");
					scanf("%ld", &id_carrinho);
					sprintf(insert, "select sum(quantidade*preco) from item left outer join produto on item.id_produto=produto.id where id_carrinho like'%ld';", id_carrinho);
					consulta(insert,1);
				break;

				case 7:/*Valor pago por um pedido: somo todos os valores de todos os pagamentos referentes à um pedido*/
					printf("id do pedido: ");
					scanf("%d", &id_pedido);
					sprintf(insert, "select sum(valor) from pedido right outer join pagamento on pedido.id=pagamento.id_pedido where pedido.id = %d;", id_pedido);
					consulta(insert, 1);
				break;

				case 8:/*Quantidade de pagamentos feitos a um pedido: peço id do pedido e conto quantas vezes esse id aparece na tabela de pagamentos*/
					printf("id do pedido: ");
					scanf("%d", &id_pedido);
					sprintf(insert, "select count(id) from pagamento where id_pedido = %d;", id_pedido);
					consulta(insert, 1);
				break;

				case 9:/*Quantidade de itens de um pedido: peço o id do pedido e somo todas as quantidades daquele pedido*/
					printf("id do pedido: ");
					scanf("%d", &id_pedido);
					sprintf(insert, "select sum(quantidade) from item where id_pedido = %d", id_pedido);
					consulta(insert, 1);
				break;

				case 10:/*Todos os clientes que compram online*/
					consulta("select nome from cliente where email like'%'", 1);
				break;

				case 11:/*Total de clientes ativos: conto todos os clientes que tem email*/
					consulta("select count(*) from cliente where status like'ativo'", 1);
				break;

				case 12:/*Estado com mais clientes: agrupo os clientes por estado e ordeno a tabela decrescente de acordo com a contagem de estados*/
					consulta("select estado, count(*) from cliente group by estado order by count(*) desc limit 1;", 1);
				break;

				case 13:/*Cliente com mais pedidos: faço uma "super tabela" com cliente, conta e pedido. Agrupo poe cliente e ordeno pra printar o que aparece mais*/
					consulta("select cpf, nome from cliente left outer join conta on cliente.cpf=conta.id_cliente left outer join pedido on conta.id=pedido.id_conta group by cliente.cpf order by count(*) desc limit 1;", 1);
				break;

				case 14:/*Produto mais comprado: faço um join de produto e item e filtro com like'%' para ter somente itens de pedidos, agrupo e ordeno para printar o que aparece mais*/
					consulta("select nome, sum(quantidade) from produto right outer join item on produto.id=item.id_produto where item.id_pedido like'%' group by produto.id order by sum(quantidade) desc limit 1;", 1);
				break;

				case 15:/*Valor médio dos pedidos: faço inner join com pedido e item. isso me dá somente os itens em pedidos. com isso, faço a média. */
					consulta("select sum(quantidade*preco_unit)/count(*) from pedido inner join item on pedido.id=item.id_pedido;", 1);
				break;
			}
		}
		else if(n == 3){/*Atualizações*/

			n = MenuAtualizar();

			switch (n){

				case 1:/*Preço de um produto. O valor inserido pode ser positivo(aumento) ou negativo(redução)*/
					printf("id do produto: ");
					scanf("%d", &id_produto);
					printf("Porcentagem da alteração: ");
					scanf("%d", &n);
					sprintf(insert,"update produto set preco = preco+preco*%d/100 where produto.id = %d", n, id_produto);
					consulta(insert, 0);
					printf("\nPreço alterado com sucesso\n");
				break;

				case 2:/*Quantidade de item no carrinho*/
					printf("id do item: ");
					scanf("%d", &id_item);
					printf("Nova quantidade: ");
					scanf("%d", &n);
					sprintf(insert,"update item set quantidade = %d where item.id = %d", n, id_item);
					consulta(insert, 0);
					printf("\nQuantidade alterada com sucesso\n");
				break;

				case 3:/*Status de um cliente*/
					printf("id do cliente: ");
					scanf("%ld", &id_cliente);
					printf("Novo status: ");
					scanf("%*c%[^\n]", status);
					sprintf(insert,"update cliente set status = '%s' where cliente.cpf = %ld", status, id_cliente);
					consulta(insert, 0);
					printf("\nStatus alterado com sucesso\n");
				break;

				case 4:/*Status de um pedido*/
					printf("id do pedido: ");
					scanf("%d", &id_pedido);
					printf("Novo status: ");
					scanf("%*c%[^\n]", status_pedido);
					sprintf(insert,"update pedido set status = '%s' where pedido.id = %d", status_pedido, id_pedido);
					consulta(insert, 0);
					printf("\nStatus alterado com sucesso\n");
				break;

				case 5:/*Endereço de um cliente*/
					printf("CPF do cliente: ");
					scanf("%ld", &id_cliente);
					printf("Bairro: ");
					scanf("%*c%[^,\n]", bairro);
					printf("Cidade: ");
					scanf("%*c%[^,\n]", cidade);
					printf("Estado: ");
					scanf("%*c%[^,\n]", estado);
					sprintf(insert, "update cliente set bairro = '%s', cidade = '%s', estado = '%s' where cliente.cpf like'%ld';", bairro, cidade, estado, id_cliente);
					consulta(insert, 0);
					printf("Endereço alterado com sucesso\n");
				break;
			}
		}
		else if(n == 4){

			n = MenuRemover();

			switch(n){

				case 1:/*Remover todos os cliente que nunca pediram e tem carrinho vazio*/
					consulta("delete from cliente where cpf not in (select id_conta from pedido) and cpf not in (select id_carrinho from item where id_carrinho=cpf)", 0);
					printf("Clientes removidos do sistema\n");
				break;

				case 2:/*Remover Todos os itens de um carrinho*/
					printf("id do carrinho: ");
					scanf("%ld", &id_carrinho);
					sprintf(insert, "delete from item where id_carrinho=%ld;", id_carrinho);
					consulta(insert, 0);
					printf("Itens removidos do carrinho\n");
				break;

				case 3:/*Remover um produto. Itens do carrinho são apagados e itens de pedidos são referenciados com null*/
					printf("id produto: ");
					scanf("%d", &id_produto);
					sprintf(insert, "delete from produto where id=%d;", id_produto);
					consulta(insert, 0);
					printf("Produto removido do sistema\n");
				break;	

				case 4:/*Remover Cliente do sistema*/
					printf("CPF do cliente: ");
					scanf("%ld", &id_cliente);
					sprintf(insert, "delete from cliente where cpf=%ld;", id_cliente);
					consulta(insert, 0);
					printf("O cliente e todos os dados relacionados a ele foram removidos do sistema\n");
				break;				
			}
		}

		printf("\nPressione enter para voltar ao menu principal\n");
		scanf("%*c%c", &aux);

	}

	return 0;
}